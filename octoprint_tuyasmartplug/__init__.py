# TODO: Fix software updater
# TODO: Protect paths in the REST API
# TODO: Make the settings revert when cancel is clicked
# TODO: Handle errors if invalid data were to be stored in the settings
# TODO: Implement permissions in web interface and API
# TODO: Is it better to keep an instance of the smart plug in the class or just reconstruct it on every call to on/off?
# TODO: Protect against retrieving the settings through custom view model (if possible)
# TODO: Make the power commands return success or failure statuses/toggle return state

# coding=utf-8
from __future__ import absolute_import

import octoprint.plugin
import pytuya


class TuyaSmartPlugPlugin(octoprint.plugin.SettingsPlugin,
						  octoprint.plugin.TemplatePlugin,
						  octoprint.plugin.SimpleApiPlugin,
						  octoprint.plugin.AssetPlugin):

	# SettingsPlugin mixin
	def get_settings_defaults(self):
		# Specify default settings for the plugin
		return dict(
			deviceId="",
			ipAddress="",
			localKey=""
		)

	# AssetPlugin mixin
	def get_assets(self):
		# Define assets to be included in the UI
		return dict(
			js=["js/tuyasmartplug.js"]  # The viewmodel for the plugin
		)

	# SimpleAPIPlugin mixin
	# This is used by the JavaScript running on the web frontend to tell the backend to send commands to the smart plug
	# Define the valid API commands
	def get_api_commands(self):
		return dict(
			powerOn=[],
			powerOff=[],
			powerToggle=[]
		)

	# Method that handles actual calls to the API
	def on_api_command(self, command, data):
		# Check the command and call the appropriate method
		if command == "powerOn":
			self.power_on()
		elif command == "powerOff":
			self.power_off()
		elif command == "powerToggle":
			self.power_toggle()

	# Below are helper methods for each API command
	# power on command
	def power_on(self):
		# Create an instance of the smart plug
		smart_plug = pytuya.OutletDevice(self._settings.get(["deviceId"]), self._settings.get(["ipAddress"]),
										 self._settings.get(["localKey"]))
		# Tell it to turn on
		smart_plug.set_status(True)

	# power off command
	def power_off(self):
		smart_plug = pytuya.OutletDevice(self._settings.get(["deviceId"]), self._settings.get(["ipAddress"]),
										 self._settings.get(["localKey"]))
		# Tell it to turn off
		smart_plug.set_status(False)

	# Toggle the power state
	def power_toggle(self):
		smart_plug = pytuya.OutletDevice(self._settings.get(["deviceId"]), self._settings.get(["ipAddress"]),
										 self._settings.get(["localKey"]))
		# Get the current power state from the plug. Assuming it is at switch position 1, as most single plugs should be
		state = smart_plug.status()['dps']['1']
		# Set the state to be the opposite of what it is now
		smart_plug.set_status(not state)

	# Get the power state from the plug


	# Softwareupdate hook
	def get_update_information(self):
		# Configure the software updater for this plugin
		return dict(
			tuyasmartplug=dict(
				# Plugin info
				displayName="Tuya Smart Plug",
				displayVersion=self._plugin_version,

				# version check: bitbucket repository
				type="bitbucket_commit",
				user="thompsonmatthew",
				repo="OctoPrint-TuyaSmartPlug",
				branch="master",

				# Update method
				pip="https://bitbucket.org/thompsonmatthew/octoprint-tuyasmartplug/get/master.zip"
			)
		)


# Override metadata from setup.py
__plugin_name__ = "Tuya Smart Plug"


def __plugin_load__():
	global __plugin_implementation__
	__plugin_implementation__ = TuyaSmartPlugPlugin()

	global __plugin_hooks__
	__plugin_hooks__ = {
		"octoprint.plugin.softwareupdate.check_config": __plugin_implementation__.get_update_information
	}
