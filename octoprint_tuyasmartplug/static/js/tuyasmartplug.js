// TODO: Potentially bind settings to your own observables
/*
 * View model for OctoPrint-TuyaSmartPlug
 *
 * Author: Matthew Thompson
 * License: AGPLv3
 */
$(function() {
    function TuyaSmartPlugViewModel(parameters) {

        // Assign the injected settings viewmodel to a property
        this.settings = parameters[0];


        // Viewmodel Functions
        // Helper method to send a JSON object to the API backend
        this.sendAjaxHelper = function(jsonObject)
        {
            // Make an AJAX POST to the plugin's API backend
            $.ajax({
                url: API_BASEURL + "plugin/tuyasmartplug",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=UTF-8",
                data: JSON.stringify(jsonObject)
            });
        };

        // Sends the command to turn on the plug
        this.powerOnAjax = function()
        {
            this.sendAjaxHelper({ command: "powerOn" });
        };

        // Sends the command to turn off the plug
        this.powerOffAjax = function()
        {
            this.sendAjaxHelper({ command: "powerOff" });
        };

        // Sends the command to toggle the plug's power state
        this.powerToggleAjax = function()
        {
            this.sendAjaxHelper({ command: "powerToggle" });
        };
    }

    // Register the viewmodel with OctoPrint
    OCTOPRINT_VIEWMODELS.push({
        construct: TuyaSmartPlugViewModel,
        // Inject the settings view model as a dependency
        dependencies: [ "settingsViewModel" ],
        // Bind to the navbar and to the settings
        elements: [ "#navbar_plugin_tuyasmartplug", "#settings_plugin_tuyasmartplug" ]
    });
});
